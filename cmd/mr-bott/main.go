package main

import (
	"bytes"
	"context"
	"fmt"
	"gitlab-bot/pkg"
	"io"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"unicode"

	"github.com/Khan/genqlient/graphql"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
)

func main() {
	_ = cmd.Execute()
}

var cmd = &cobra.Command{
	Use:   "mr-bot [project] [id]",
	Short: "Process merge request rules",
	Args:  cobra.ExactArgs(2),
	Run:   run,
}

var reTitle = regexp.MustCompile(`^(.*?)( ?\[(?:\w+-|#)\d+\])?$`)
var reDescTask = regexp.MustCompile(`(?i)\b(closes|updates) (\w+-|#)\d+\b`)
var reDescClType = regexp.MustCompile(`(^|\n)(?i)Changelog: (\w+)($|\n)`)

func run(_ *cobra.Command, args []string) {
	project := args[0]
	iid, err := strconv.ParseInt(args[1], 10, 64)
	check(err)

	gl, err := gitlab.NewClient(os.Getenv("ACCESS_TOKEN"))
	check(err)

	cu, _, err := gl.Users.CurrentUser()
	check(err)

	client := graphql.NewClient("https://gitlab.com/api/graphql", http.DefaultClient)
	res, err := pkg.GetNotes(context.Background(), client, project, strconv.FormatInt(iid, 10))
	checkf(err, "query GitLab")

	mr := res.Project.MergeRequest
	if mr.State == "" {
		fatalf("Merge request %d not found", iid)
	}

	if mr.State != "opened" {
		fmt.Fprintf(os.Stderr, "MR is not open, nothing to do")
		return
	}

	// Check for approval
	var didApprove bool
	for _, a := range mr.ApprovedBy.Nodes {
		if a.Username == cu.Username {
			didApprove = true
		}
	}

	// Find existing checklist
	var existingNID int64
	var existingDID string
	for _, note := range mr.Notes.Nodes {
		switch {
		case note.System:
			continue
		case note.Author.Username == cu.Username:
			// Ok
		default:
			continue
		}
		if strings.Contains(note.Body, "<!-- Mr. Bott // Checklist -->") {
			nid := parseId(note.Id)
			did := splitId(note.Discussion.Id)
			if existingNID > 0 {
				_, err = gl.Discussions.DeleteMergeRequestDiscussionNote(project, int(iid), did, int(nid))
				check(err)
			} else {
				existingNID, existingDID = nid, did
			}
		}
	}

	// Check for "ignore me"
	var releaseMerge bool
	for _, l := range mr.Labels.Nodes {
		switch l.Title {
		case "mr-bott:ignore":
			// If the MR is marked ignore-me after being approved, unapprove it
			if didApprove {
				_, err = gl.MergeRequestApprovals.UnapproveMergeRequest(project, int(iid), nil)
				check(err)
			}

			// Delete the comment
			if existingNID > 0 {
				_, err = gl.Discussions.DeleteMergeRequestDiscussionNote(project, int(iid), existingDID, int(existingNID))
				check(err)
			}
			return

		case "release merge":
			releaseMerge = true
		}
	}

	matches := reTitle.FindStringSubmatch(mr.Title)
	if matches == nil {
		fatalf("title: no matches")
	}
	rules.MustMark("title-form", checkTitleForm(matches[1]))
	rules.MustMark("title-task", releaseMerge || matches[2] != "")

	rules.MustMark("desc-task", releaseMerge || reDescTask.MatchString(mr.Description))
	rules.MustMark("desc-cltype", reDescClType.MatchString(mr.Description))

	rules.MustMark("val-label", checkValLabels(mr))

	okBeforeTarget := rules.State != nil && *rules.State
	rules.MustMark("target", checkTarget(gl, mr, project))
	okAfterTarget := rules.State != nil && *rules.State

	buf := new(bytes.Buffer)
	// fmt.Fprintf(buf, "### Checklist\n")
	fmt.Fprintf(buf, "<!-- Mr. Bott // Checklist -->\n")
	rules.Print(buf, "")
	var mrLabels gitlab.Labels
	switch {
	case okAfterTarget:
		fmt.Fprint(buf, "\nChecklist is complete! 🎉\n")
		mrLabels = gitlab.Labels{"checklist::done"}

	case okBeforeTarget:
		fmt.Fprint(buf, "\nMerge request is blocked\n")
		mrLabels = gitlab.Labels{"checklist::blocked"}

	default:
		fmt.Fprintf(buf, "\n@%s attention is needed!\n", mr.Author.Username)
		mrLabels = gitlab.Labels{"checklist::attention"}
	}

	// Set labels
	_, _, err = gl.MergeRequests.UpdateMergeRequest(project, int(iid), &gitlab.UpdateMergeRequestOptions{AddLabels: &mrLabels})
	check(err)

	// Create or update the discussion
	s := buf.String()
	if existingNID > 0 {
		_, _, err = gl.Discussions.UpdateMergeRequestDiscussionNote(project, int(iid), existingDID, int(existingNID), &gitlab.UpdateMergeRequestDiscussionNoteOptions{Body: &s})
		check(err)
	} else {
		d, _, err := gl.Discussions.CreateMergeRequestDiscussion(project, int(iid), &gitlab.CreateMergeRequestDiscussionOptions{Body: &s})
		check(err)
		existingDID = d.ID
	}

	// Resolve or unresolve the discussion
	_, _, err = gl.Discussions.ResolveMergeRequestDiscussion(project, int(iid), existingDID, &gitlab.ResolveMergeRequestDiscussionOptions{Resolved: &okAfterTarget})
	check(err)

	// Approve or unapprove the MR
	switch {
	case okAfterTarget && !didApprove:
		_, _, err = gl.MergeRequestApprovals.ApproveMergeRequest(project, int(iid), nil)
		check(err)
	case !okAfterTarget && didApprove:
		_, err = gl.MergeRequestApprovals.UnapproveMergeRequest(project, int(iid), nil)
		check(err)
	}

	// Update MR assignees
	issues, _, err := gl.MergeRequests.GetIssuesClosedOnMerge(project, int(iid), nil)
	check(err)
	if len(mr.Assignees.Nodes) == 0 {
		for _, i := range issues {
			if len(i.Assignees) == 0 {
				continue
			}

			var ids []int
			for _, a := range i.Assignees {
				ids = append(ids, a.ID)
			}

			_, _, err = gl.MergeRequests.UpdateMergeRequest(project, int(iid), &gitlab.UpdateMergeRequestOptions{AssigneeIDs: &ids})
			check(err)
			break
		}
	}

	// Update issue status and assignees
	var issueLabels gitlab.Labels
	if mr.Draft {
		issueLabels = gitlab.Labels{"status::in progress"}
	} else {
		issueLabels = gitlab.Labels{"status::review"}
	}
	var issueAssignees []int
	for _, a := range mr.Assignees.Nodes {
		parts := strings.Split(a.Id, "/")
		id, err := strconv.ParseInt(parts[len(parts)-1], 10, 64)
		checkf(err, "merge request assignee has invalid ID")
		issueAssignees = append(issueAssignees, int(id))
	}
	for _, i := range issues {
		if i.IID == 0 {
			continue
		}
		opts := &gitlab.UpdateIssueOptions{AddLabels: &issueLabels}
		if len(issueAssignees) > 0 {
			opts.AssigneeIDs = &issueAssignees
		}
		_, _, err = gl.Issues.UpdateIssue(project, i.IID, opts)
		check(err)
	}
}

type Rule struct {
	Name, Text string
	State      *bool
	Rules      []*Rule
}

var rules = &Rule{
	Rules: []*Rule{
		{
			Text: "Merge request titles must be `Description [task]`",
			Rules: []*Rule{
				{Name: "title-form", Text: "The description must be imperative, short, and start with a capital"},
				{Name: "title-task", Text: "The title must reference a task or be a ~\"release merge\""},
				{Text: "For example, `Add cool feature [#123]`"},
			},
		},
		{
			Text: "The description must reference a task and a changelog type",
			Rules: []*Rule{
				{Name: "desc-task", Text: "The description must close or update a task, such as `Closes #123` or `Updates #123` or be a ~\"release merge\""},
				{Name: "desc-cltype", Text: "The description must specify a changelog type, such as `Changelog: feature`"},
			},
		},
		{
			Text: "The description must include a description of the changes",
			Rules: []*Rule{
				{Text: "It is preferable for the description to be written in present tense"},
				{Text: "For example, \"Implements a cool feature by modifying the API\""},
			},
		},
		{
			Text: "Changes must be validated by one or more tests that are run during CI",
			Rules: []*Rule{
				{Text: "Validation is not applicable to things like documentation updates"},
				{Text: "Purely UI/UX changes can be manually validated, such as changes to human-readable output"},
				{Text: "Basically everything else must be validated by CI"},
			},
		},
		{
			Name: "val-label",
			Text: "Merge requests must be marked with one of the validation labels",
			Rules: []*Rule{
				{Text: "~\"validation::ci/cd\" for changes validated by CI tests"},
				{Text: "~\"validation::manual\" for changes validated by hand"},
				{Text: "~\"validation::deferred\" for changes validated by a follow up merge request"},
				{Text: "~\"validation::not applicable\" for changes where validation is not applicable"},
			},
		},
		{
			Name: "target",
			Text: "Merge target is a protected branch or is labeled ~\"allow branch merge\"",
		},
	},
}

func (r *Rule) MustMark(name string, state bool) {
	if !r.Mark(name, state) {
		fatalf("unknown rule %s", name)
	}
}

func (r *Rule) Mark(name string, state bool) bool {
	if r.Name == name {
		r.State = &state
		return true
	}
	for _, rr := range r.Rules {
		if rr.Mark(name, state) {
			if r.State == nil {
				r.State = &state
			} else if *r.State && !state {
				*r.State = false
			}
			return true
		}
	}
	return false
}

func (r *Rule) Print(w io.Writer, indent string) {
	var state string
	if r.Name != "" {
		if r.State == nil || !*r.State {
			state = "[ ] "
		} else if *r.State {
			state = "[x] "
		}
	}
	if r.Text != "" {
		fmt.Fprintf(w, "%s* %s%s\n", indent, state, r.Text)
		indent += "  "
	}
	for _, r := range r.Rules {
		r.Print(w, indent)
	}
}

func checkTitleForm(s string) bool {
	for _, r := range s {
		return unicode.IsUpper(r)
	}
	return false
}

func checkValLabels(mr pkg.GetNotesProjectMergeRequest) bool {
	for _, l := range mr.Labels.Nodes {
		if strings.HasPrefix(l.Title, "validation::") {
			return true
		}
	}
	return false
}

func checkTarget(gl *gitlab.Client, mr pkg.GetNotesProjectMergeRequest, project string) bool {
	for _, l := range mr.Labels.Nodes {
		if l.Title == "allow branch merge" {
			return true
		}
	}
	br, _, err := gl.Branches.GetBranch(project, mr.TargetBranch)
	checkf(err, "check target branch")
	return br.Protected
}

func fatalf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "Error: "+format+"\n", args...)
	os.Exit(1)
}

func check(err error) {
	if err != nil {
		fatalf("%v", err)
	}
}

func checkf(err error, format string, otherArgs ...interface{}) {
	if err != nil {
		fatalf(format+": %v", append(otherArgs, err)...)
	}
}

func splitId(s string) string {
	i := strings.LastIndexByte(s, '/')
	if i < 0 {
		fatalf("invalid ID")
	}
	return s[i+1:]
}

func parseId(s string) int64 {
	id, err := strconv.ParseInt(splitId(s), 10, 64)
	checkf(err, "invalid ID")
	return id
}
