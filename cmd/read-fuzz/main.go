package main

import (
	"archive/zip"
	"bytes"
	"context"
	"fmt"
	"gitlab-bot/pkg"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/Khan/genqlient/graphql"
	"github.com/spf13/cobra"
)

var cmd = &cobra.Command{
	Use:   "read-fuzz [count]",
	Short: "Read the results of fuzz tests",
	Args:  cobra.ExactArgs(1),
	Run:   run,
}

func main() {
	_ = cmd.Execute()
}

func run(_ *cobra.Command, args []string) {
	count, err := strconv.ParseInt(args[0], 10, 64)
	check(err)

	fmt.Println("Querying GitLab")
	client := graphql.NewClient("https://gitlab.com/api/graphql", http.DefaultClient)
	res, err := pkg.ReadFuzz(context.Background(), client, "accumulatenetwork/accumulate", "master", int(count))
	checkf(err, "query GitLab")

	for _, pl := range res.Project.Pipelines.Nodes {
		for _, job := range pl.Jobs.Nodes {
			if !strings.HasPrefix(job.Name, "fuzz: [") || !strings.HasSuffix(job.Name, "]") {
				continue
			}

			parts := strings.Split(job.Name[len("fuzz: ["):len(job.Name)-len("]")], ",")
			if len(parts) != 2 {
				continue
			}
			jobFile, jobFunc := strings.TrimSpace(parts[0]), strings.TrimSpace(parts[1])
			if path.IsAbs(jobFile) {
				fatalf("absolute path %q", jobFile)
			}

			var dlArchive string
			for _, artifact := range job.Artifacts.Nodes {
				switch artifact.FileType {
				// case "COVERAGE_FUZZING":
				// 	dlReport = "https://gitlab.com/" + artifact.DownloadPath
				case "ARCHIVE":
					dlArchive = "https://gitlab.com/" + artifact.DownloadPath
				}
			}

			if dlArchive == "" {
				continue
			}

			// Fetch crashers from the archive
			fmt.Printf("Fetching artifacts of https://gitlab.com/%s\n", job.WebPath)
			resp, err := http.Get(dlArchive)
			check(err)
			if resp.ContentLength > 1<<24 {
				log.Printf("Job %v has an artifact that is too big", job)
				continue
			}

			b, err := io.ReadAll(resp.Body)
			check(err)
			check(resp.Body.Close())

			r, err := zip.NewReader(bytes.NewReader(b), resp.ContentLength)
			check(err)

			for _, f := range r.File {
				if f.FileInfo().IsDir() || !strings.HasPrefix(f.Name, "crashes/") {
					continue
				}

				dir := strings.Split(jobFile, "/")
				dir[len(dir)-1] = "testdata"
				dir = append(dir, "fuzz", jobFunc)
				dirname := filepath.Join(dir...)
				check(os.MkdirAll(dirname, 0755))

				filename := filepath.Join(dirname, path.Base(f.Name))
				fmt.Printf("Downloading %s (https://gitlab.com/%s)\n", filename, job.WebPath)
				dst, err := os.Create(filename)
				check(err)
				src, err := f.Open()
				check(err)
				_, err = io.Copy(dst, src)
				check(err)
			}
		}
	}
}

func fatalf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "Error: "+format+"\n", args...)
	os.Exit(1)
}

func check(err error) {
	if err != nil {
		fatalf("%v", err)
	}
}

func checkf(err error, format string, otherArgs ...interface{}) {
	if err != nil {
		fatalf(format+": %v", append(otherArgs, err)...)
	}
}
