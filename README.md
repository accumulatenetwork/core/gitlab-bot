# GitLab Bots

Bots that manage Accumulate core development's GitLab assets.

## Mr. Bott

Mr. Bott is a bot that verifies that merge requests meet the required
conditions.

* Merge request titles must be `Description [task]`
  * The description must be imperative, short, and start with a capital
  * The title must reference a task
  * For example, `Add cool feature [AC-123]`
* The description must reference a task and a changelog type
  * The description must close or update a task, such as `Closes AC-123` or `Updates AC-123`
  * The description must specify a changelog type, such as `Changelog: feature`
* The description must include a description of the changes
  * It is preferable for the description to be written in present tense
  * For example, "Implements a cool feature by modifying the API"
* Changes must be validated by one or more tests that are run during CI
  * Validation is not applicable to things like documentation updates
  * Purely UI/UX changes can be manually validated, such as changes to human-readable output
  * Basically everything else must be validated by CI
* Merge requests must be marked with one of the validation labels
  * ~"validation::ci/cd" for changes validated by CI tests
  * ~"validation::manual" for changes validated by hand
  * ~"validation::deferred" for changes validated by a follow up merge request
  * ~"validation::not applicable" for changes where validation is not applicable